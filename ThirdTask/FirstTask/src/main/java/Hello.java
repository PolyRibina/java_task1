import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Hello {
    public static void main(String[] args) throws IOException, ParseException{

        Vector v1 = new Vector(3.0, 1.0, 3.0);
        Vector v2 = new Vector(2.0, 4.0, 3.0);

        System.out.println("Длина вектора 1 = " + v1.VectorLength());
        System.out.println("Длина вектора 2 = " + v2.VectorLength());

        System.out.println("Скалярное произведение векторов = " + v1.ScalarProduct(v2));
        System.out.println("Векторное произведение векторов = " + v1.СrossProduct(v2).getCoord());

        System.out.println("Угол между векторами = " + v1.AngleBetweenVectors(v2));

        System.out.println("Сумма векторов = " + v1.VectorAddition(v2).getCoord());
        System.out.println("Разность векторов = " + v1.VectorDifference(v2).getCoord());

    }
}
