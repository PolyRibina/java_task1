import java.util.Random;



public class Vector {
    private final Double X;
    private final Double Y;
    private final Double Z;
    static final Random random = new Random();

    public Vector(Double X, Double Y, Double Z) //
    {
        this.X = X;
        this.Y = Y;
        this.Z = Z;
    }

    public String getCoord()
    {
        return (X + " " + Y + " " + Z);
    }

    public Double getX()
    {
        return X;
    }
    public Double getY()
    {
        return Y;
    }
    public Double getZ()
    {
        return Z;
    }

    public Double VectorLength() //  Метод вычисляет длину вектора
    {
        return Math.sqrt(getX()*getX() + getY()*getY() + getZ()*getZ());
    }

    public Double ScalarProduct(Vector v) // Метод вычисляет скалярное произведение с другим вектором
    {
        return getX()*v.getX() + getY()*v.getY() + getZ()*v.getZ();
    }

    public Vector СrossProduct(Vector a) // Метод вычисляет векторное произведение с другим вектором
    {
        return new Vector((-a.getY()*getZ() + a.getZ()*getY()), (-a.getZ()*getX() + a.getX()*getZ()), (-a.getX()*getY() + a.getY()*getX()));
    }

    public Double AngleBetweenVectors(Vector v) //  Метод вычисляет угол между векторами
    {
        return ScalarProduct(v)/(v.VectorLength()*VectorLength());
    }

    public Vector VectorAddition(Vector v) // Методы вычисляет сумму
    {
        return new Vector(getX() + v.getX(), getY()+v.getY(), getZ() + v.getZ());
    }

    public Vector VectorDifference(Vector v) // Методы вычисляет разность
    {
        return new Vector(getX() - v.getX(), getY() - v.getY(), getZ() - v.getZ());
    }

    public static Vector[] RandomVectors(int N) // Метод возвращает массив случайных векторов размера N
    {
        Vector[] masV = new Vector[N];
        for (Vector v : masV)
        {
            v = new Vector(random.nextDouble(), random.nextDouble(), random.nextDouble());
        }
        return masV;
    }
}
