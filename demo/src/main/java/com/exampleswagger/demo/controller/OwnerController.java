package com.exampleswagger.demo.controller;

import com.exampleswagger.demo.model.OwnerModel;
import com.exampleswagger.demo.service.OwnerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController

@RequiredArgsConstructor
public class OwnerController {
    private final OwnerService ownerService;

    @PostMapping("/addOwner") // добавление
    public void Add(@RequestBody(required = true) OwnerModel person){
        ownerService.Add(person);
    }

    @PatchMapping("/editOwner") // изменение
    public void EditOwner(OwnerModel person){
        ownerService.EditOwner(person);
    }

    @DeleteMapping("/deleteOwner") // удаление
    public void DeleteOwner(Long id){
        ownerService.DeleteOwner(id);
    }

    @GetMapping("/getOwner")
    public OwnerModel GetOwner(){
        return ownerService.currentPerson();
    }

    @GetMapping("/getOwnerById/set/{id}")
    public OwnerModel GetOwnerById(@PathVariable("id") Long id){ // @PathVariable - отправляем запрос
        return ownerService.GetOwnerById(id);
    }

    @GetMapping("/getOwnerByNumber/set/{number}")
    public OwnerModel GetOwnerByNumber(@PathVariable("number") String number){
        return ownerService.GetOwnerByNumber(number);
    }

    @GetMapping("/getOwnerByNameFIO/set/{name}")
    public OwnerModel GetOwnerByNameFIO(@PathVariable("name") String model){
        return ownerService.GetOwnerByNameFIO(model);
    }

    @GetMapping("/getOwnersByAge/set/{age}")
    public List<OwnerModel> GetOwnersByAge(@PathVariable("age") Integer age){
        return ownerService.GetOwnersByAge(age);
    }
}
