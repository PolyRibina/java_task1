package com.exampleswagger.demo.controller;

import com.exampleswagger.demo.model.AccountingOfCarsModel;
import com.exampleswagger.demo.service.AccountingOfCarsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AccountingOfCarsController {

    private final AccountingOfCarsService carsService;

    @PostMapping("/addCar") // добавление
    public void AddCar(@RequestBody(required = true) AccountingOfCarsModel car){
        carsService.AddCar(car);
    }

    @PatchMapping("/editCar") // изменение
    public void EditCar(AccountingOfCarsModel car){
        carsService.EditCar(car);
    }

    @DeleteMapping("/deleteCar") // удаление
    public void DeleteCar(Long id){
        carsService.DeleteCar(id);
    }

    @GetMapping("/getCar")
    public AccountingOfCarsModel GetCar(){
        return carsService.currentCar(); // сделать бд и подлючить в propeties, autowired
    }

    @GetMapping("/getCarById/set/{id}")
    public AccountingOfCarsModel GetCarById(@PathVariable("id") Long id){ // @PathVariable - отправляем запрос
        return carsService.GetCarById(id);
    }

    @GetMapping("/getCarByNumber/set/{number}")
    public AccountingOfCarsModel GetCarByNumber(@PathVariable("number") String number){
        return carsService.GetCarByNumber(number);
    }

    @GetMapping("/getCarsByModel/set/{model}")
    public List<AccountingOfCarsModel> GetCarsByModel(@PathVariable("model") String model){
        return carsService.GetCarsByModel(model);
    }
}
