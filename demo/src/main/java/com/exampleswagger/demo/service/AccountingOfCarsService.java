package com.exampleswagger.demo.service;

import com.exampleswagger.demo.model.AccountingOfCarsModel;
import com.exampleswagger.demo.repository.AccountingOfCarsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;



@Service
@RequiredArgsConstructor

public class AccountingOfCarsService {

    private final AccountingOfCarsRepository carsRepository;

    public AccountingOfCarsModel currentCar(){
        var car = new AccountingOfCarsModel();
        car.setId((long) 1);
        car.setColor("black");
        car.setModel("sedan");
        car.setNumber("0abc07");
        car.setState("good");
        return car;
    }

    public void AddCar(AccountingOfCarsModel car){
        carsRepository.save(car);
    }

    public void DeleteCar(Long id){
        carsRepository.deleteById(id);
    }

    public void EditCar(AccountingOfCarsModel car){
        var model = GetCarById(car.getId());

        if(!car.getModel().isEmpty()){
            model.setModel(car.getModel());
        }
        if(!car.getNumber().isEmpty()){
            model.setNumber(car.getNumber());
        }
        if(!car.getColor().isEmpty()){
            model.setColor(car.getColor());
        }
        if(!car.getState().isEmpty()){
            model.setState(car.getState());
        }
        AddCar(model);
    }

    public AccountingOfCarsModel GetCarById(Long Id){
        return carsRepository.findById(Id);
    }

    public AccountingOfCarsModel GetCarByNumber(String number){
        return carsRepository.findByNumber(number);
    }

    public List<AccountingOfCarsModel> GetCarsByModel(String model){
        return carsRepository.findByModel(model);
    }
}
