package com.exampleswagger.demo.service;

import com.exampleswagger.demo.model.OwnerModel;
import com.exampleswagger.demo.repository.OwnerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OwnerService {

    private final OwnerRepository ownerRepository;

    // будет отправлять сообщение на почту
    @Bean
    @Transactional
    public void getJavaMailSender() {
        EmailServiceImpl emailService = new EmailServiceImpl();
        emailService.sendSimpleMessage(currentPerson().getEmail(), "Your car in good state!");
    }

    public OwnerModel currentPerson(){
        var person = new OwnerModel();
        person.setId((long) 1);
        String str= "Иванов Иван Иваныч";
        person.setNameFIO(str);
        person.setAge(25);
        person.setEmail("ivanjv@yandex.ru");
        person.setNumberPhone("88005553535");
        return person;
    }

    public void Add(OwnerModel person){
        ownerRepository.save(person);
    }

    public void DeleteOwner(Long id){
        ownerRepository.deleteById(id);
    }

    public void EditOwner(OwnerModel owner){
        var model = GetOwnerById(owner.getId());

        if(!owner.getNameFIO().isEmpty()){
            model.setNameFIO(owner.getNameFIO());
        }
        if(!owner.getNumberPhone().isEmpty()){
            model.setNumberPhone(owner.getNumberPhone());
        }
        if(!owner.getEmail().isEmpty()){
            model.setEmail(owner.getEmail());
        }

        Add(model);
    }

    public OwnerModel GetOwnerById(Long Id){
        return ownerRepository.findById(Id);
    }

    public OwnerModel GetOwnerByNumber(String number){
        return ownerRepository.findByNumberPhone(number);
    }

    public OwnerModel GetOwnerByNameFIO(String name){
        return ownerRepository.findByNameFIO(name);
    }

    public List<OwnerModel> GetOwnersByAge(Integer age){
        return ownerRepository.findByAge(age);
    }
}
