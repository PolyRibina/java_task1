package com.exampleswagger.demo.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ownerModel")
public class OwnerModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String nameFIO;
    private String numberPhone;
    private String email;
    private Integer age;


    @OneToMany(mappedBy = "model")
    private List<AccountingOfCarsModel> carsModels;

    public void setId(Long i) {
        Id = i;
    }
    public Long getId() {
        return Id;
    }


    public void setNameFIO(String str) {
        nameFIO = str;
    }
    public String getNameFIO() {
        return nameFIO;
    }

    public void setAge(int i) {
        age = i;
    }
    public int getAge() {
        return age;
    }

    public void setEmail(String s) {
        email = s;
    }

    public String getEmail() {
        return email;
    }

    public void setNumberPhone(String s) {
        numberPhone = s;
    }
    public String getNumberPhone() {
        return numberPhone;
    }
}
