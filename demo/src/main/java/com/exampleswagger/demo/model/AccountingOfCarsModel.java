package com.exampleswagger.demo.model;

import com.exampleswagger.demo.service.AccountingOfCarsService;


import javax.persistence.*;
import java.util.Collection;


@Entity
@Table(name = "carModel")
public class AccountingOfCarsModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    // добавить many to one
    private Long Id;

    private String model;
    private String number;
    private String color;
    private String state;

    @ManyToOne
    private OwnerModel modelOw;

    public void setId(Long i) {
        this.Id = i;
    }

    public void setColor(String black) {
        color = black;
    }

    public void setModel(String sedan) {
        model = sedan;
    }

    public void setNumber(String s) {
        number = s;
    }

    public void setState(String good) {
        state = good;
    }

    public Long getId() {
        return Id;
    }

    public String getModel() {
        return model;
    }

    public String getNumber() {
        return number;
    }

    public String getColor() {
        return color;
    }

    public String getState() {
        return state;
    }
}
