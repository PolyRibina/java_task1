package com.exampleswagger.demo.repository;

import com.exampleswagger.demo.model.AccountingOfCarsModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountingOfCarsRepository extends JpaRepository<AccountingOfCarsModel, Integer>,
        JpaSpecificationExecutor<AccountingOfCarsModel> {

    AccountingOfCarsModel save(AccountingOfCarsModel car);


    void deleteById(Long id);

    List<AccountingOfCarsModel> findAll();

    AccountingOfCarsModel findById(Long id);

    AccountingOfCarsModel findByNumber(String number);

    List<AccountingOfCarsModel> findByModel(String model);


}
