package com.exampleswagger.demo.repository;

import com.exampleswagger.demo.model.AccountingOfCarsModel;
import com.exampleswagger.demo.model.OwnerModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OwnerRepository extends JpaRepository<OwnerModel, Integer>,
        JpaSpecificationExecutor<OwnerModel> {


    OwnerModel save(OwnerModel owner);

    void deleteById(Long id);

    List<OwnerModel> findAll();

    OwnerModel findById(Long id);

    OwnerModel findByNameFIO(String name);

    OwnerModel findByNumberPhone(String numberPhone);

    List<OwnerModel> findByAge(Integer age);


}
