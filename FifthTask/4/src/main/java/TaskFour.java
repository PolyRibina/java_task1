import java.io.IOException;
import java.text.ParseException;
import java.util.*;

public class TaskFour {
    public static void main(String[] args) throws IOException, ParseException {

        Map<Object, Object> map = new HashMap<Object, Object>(); // создаем объект типа Map<Object, Object> для универсальности

        // Заполняем map содержимым
        map.put(1, "Cat");
        map.put(4, "Dog");
        map.put(3, "Unicorn");
        map.put(5, "Unicorn");
        map.put(6, "Unicorn");
        map.put(2, "Hamster");

        map = Revers(map); // Вызываем метод, который меняет местами ключи и значения

        Write(map); // Вызываем метод, который выведет на экран map
    }

    public static Map<Object, Object> Revers(Map<Object, Object> map0){

        Map<Object, Object> mapResult = new HashMap<Object, Object>();

        for(Object i: map0.keySet()){ // Проходим по всем ключам map0

            Collection<Object> keys; // объявляем коллекцию

            if(mapResult.containsKey(map0.get(i))) { // Если mapResult содержит значение с ключом

                keys = (ArrayList<Object>)mapResult.get(map0.get(i)); // достаем коллекциию из значения по ключу
                keys.add(i); // добавляем в коллекцию новое значение

                mapResult.put(map0.get(i), keys); // заменяем поле в mapResult с тем же ключом, но новым значением
            }
            else{
                keys = new ArrayList<Object>(); // создаем новую коллекцию
                keys.add(i); // добавляем в коллекцию значение
                mapResult.putIfAbsent(map0.get(i), keys); // добавляем в mapResult новое поле ключ-значение
            }
        }

        return mapResult;
    }

    public static void Write(Map<Object, Object> map){

        // Проходим по всем элементам map и выводим ключ и значение
        for(Object i: map.keySet()){
        System.out.println(i + " " + map.get(i));
        }
    }
}
