import java.util.NoSuchElementException;

public class SuperIterator <E>{

    private int indexRow;
    private int indexСolumn;
    private E[][] mas;


    public SuperIterator(E[][] mas1){
        indexRow = 0;
        indexСolumn = 0;
        mas = mas1;
    }

    public E next() { // метод возвращает текущий элемент и переходит к следующему

        if (!hasNext() && indexRow == mas.length-1) { // если элемент последний
            indexRow++;
            return mas[indexRow-1][indexСolumn];
        }
        else if(!hasNext()){ // если дошли до конца
            throw new NoSuchElementException("Конец");
        }
        else{ // если еще есть элементы

            E now = mas[indexRow][indexСolumn];
            if (!NextRow())
                indexСolumn++;

            return now;
        }
    }

    public boolean hasNext(){ // метод возвращает true, если существует следующий элемент, и false, если нет
        int len = mas[0].length;
        return indexRow*len + indexСolumn + 1 < mas.length*len; // получаем текущую позицию и сравниваем с количеством элементов
    }

    public void remove(){ // метод удаляет текущий элемент и переходит к следующему элементу
        mas[indexRow][indexСolumn] = null;

        if(!NextRow())
            indexСolumn++;
    }

    private boolean NextRow(){ // переходим на следующую строку. Если перешли, возвращаем true
        if(indexСolumn >= mas[indexRow].length-1){
            if(indexRow < mas.length-1){
                indexRow++;
                indexСolumn = 0;
                return true;
            }
        }
        return false;
    }
}
