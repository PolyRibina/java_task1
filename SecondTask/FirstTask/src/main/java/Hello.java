import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Hello {
    public static void main(String[] args) throws IOException, ParseException{

        int count = 0; // Количество ограничений

        // Вводим с клавиатуры свою дату
        System.out.print("Введите дату в формате ГГГГ ММ ДД: ");
        BufferedReader getData = new BufferedReader(new InputStreamReader(System.in));
        String title = getData.readLine();
        DateFormat df = new SimpleDateFormat("yyyy MM dd");
        Date date = new Date();
        try {
            date =  df.parse(title);
        }
        catch (ParseException у){
            System.out.println("Ошибка! Неверный формат даты.");
            System.exit(-1);
        }

        // Вводим с клавиатуры путь к файлу
        System.out.print("Введите путь к файлу: ");
        title = getData.readLine();
        try {
            File file = new File(title); //создаем объект FileReader для объекта File
            FileReader fr = new FileReader(file); //создаем BufferedReader с существующего FileReader для построчного считывания
            BufferedReader reader = new BufferedReader(fr); // считаем сначала первую строку

            String line = reader.readLine(); // Пропускаем первую строку с названиями колонок
            line = reader.readLine();
            String[] splitted;

            while (line != null) {
                splitted = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");

                DateFormat dateInterval = new SimpleDateFormat("yyyyMMdd");
                Date startDate =  dateInterval.parse(splitted[10]);
                Date endDate =  dateInterval.parse(splitted[11]);

                if(startDate.before(date) && date.before(endDate)){ // Если дата находится в промежутке, увеличиваем количество ограничений
                    count++;
                }

                line = reader.readLine(); //считываем остальные строки в цикле
            }
        }
        catch (java.io.FileNotFoundException e){
            System.out.println("Ошибка! Укажите верный путь к файлу. Например, /Users/Полина/Desktop/data.csv");
            System.exit(-1);
        }

        System.out.println("Количество ограничений: " + count);
    }
}
